// all used environment variables in one file
import { env } from 'process';

/** run the graphqlservice on this port */
export const PORT = env.PORT || 3000;
/** loglevel: (trace) 0 - 5 (off) */
export const LOGLEVEL = Number(env.LOGLEVEL) || 1;