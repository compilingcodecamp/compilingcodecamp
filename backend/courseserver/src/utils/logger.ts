import { LogUpTsSync } from 'logupts';
import { LOGLEVEL } from './env';

export const logger = new LogUpTsSync({
    logLevel: LOGLEVEL
});