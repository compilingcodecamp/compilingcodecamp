import { IServerConfig } from './types/serverconfig';
import { Lesson } from './types/lesson';
import { Task } from './types/task';
export const demoServerConfig: IServerConfig = {
    tasks: [
        new Lesson({
            id: 'demo-1-1',
            description: 'write console.log("hello world")',
            src: `console.log("");`,
            title: 'Hello world',
            tasks: []
        }),
        new Lesson({
            id: 'demo-1-2',
            description: 'variable string',
            src: ``,
            title: 'Variables',
            tasks: [
                new Task({
                    shortDescription: 'short',
                    description: 'long', 
                }),
            ],
        })
    ]
}