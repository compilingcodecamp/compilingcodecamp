import { Lesson } from './lesson';

export interface IServerConfig {
    tasks: Lesson[];
}