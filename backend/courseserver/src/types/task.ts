import { ObjectType, Field } from 'type-graphql';
import { v4 as uuid } from 'uuid';

@ObjectType()
export class Task {

    @Field(type => String)
    description: string = '';


    @Field(type => String)
    id: string = '';


    @Field(type => String)
    shortDescription: string = '';

    @Field(type => String, {nullable: true})
    test: string = '';

    constructor(args: {
        description: string,
        shortDescription: string,
        test?: string,
    }) {
        Object.assign(this, args);
        this.id = uuid();
    }

}