import { ObjectType, Field } from 'type-graphql';
import { Task } from './task';

// id counter
let counter: number = 0;

@ObjectType()
export class Lesson {

    static all = new Map<string, Lesson>();


    @Field(type => String, {
        description: 'get task from server'
    })
    description: string = '';

    @Field(type => String, {
        description: 'Task ID: <courseid: string>-<chapter: number>-<lesson: number>'
    })
    id: string = '';

    @Field(type => String)
    src: string = '';

    @Field(type => [Task])
    tasks: Task[] = [];

    @Field(type => String, {
        description: 'task title'
    })
    title: string = '';



    constructor(args: {description: string, id: string, title: string, tasks: Task[], src: string}) {
        // assign args
        Object.assign(this, args);
        Lesson.all.set(this.id, this)
    }
}