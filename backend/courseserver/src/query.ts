import { Resolver as Res, Query } from 'type-graphql';
import { Lesson } from './types/lesson';
@Res()
export class Resolver {
    @Query(of => [Lesson])
    lessons() {
        return Lesson.all.values();
    }
}