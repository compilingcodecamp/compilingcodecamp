import 'reflect-metadata';
import { resolve } from 'path';
import { ApolloServer } from 'apollo-server';
import { buildSchema } from 'type-graphql';
import { Resolver } from './query';
import { PORT } from './utils/env';
import { logger } from './utils/logger';
import { IServerConfig } from './types/serverconfig';






export async function createCourseServer(serverconfig: IServerConfig) {
    logger.debug('build schema');
    const schema = await buildSchema({
        resolvers: [Resolver],
        emitSchemaFile: resolve(__dirname, '../schema.gql'),
    });

    const server = new ApolloServer({
        schema,
    });

    await server.listen(PORT);
}

// rum demo server, if started directly
if (!module.parent) {
    (async () => {
        logger.info('start demo courseserver');
        await createCourseServer((await import('./demo')).demoServerConfig);
        logger.info(`started on port ${PORT}`);
    })();
}