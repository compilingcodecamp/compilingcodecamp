FROM node:lts-slim

WORKDIR /build

COPY backend ./backend
COPY frontend ./frontend

COPY *.json ./
COPY yarn.lock .

RUN yarn install
RUN yarn run lerna run build
