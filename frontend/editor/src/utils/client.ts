import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

const wsLink = new WebSocketLink({
    uri: `ws://${process.env.COURSE_SERVER_URL}/` || 'ws://localhost:3000/'
});

const httpLink = new HttpLink({
    uri: `http://${process.env.COURSE_SERVER_URL}/` || 'http://localhost:3000/'
})

export const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: split(
        ({ query }) => {
            const definition = getMainDefinition(query);
            return (
                definition.kind === 'OperationDefinition' &&
                definition.operation === 'subscription'
            );
        },
        wsLink,
        httpLink
    ),
});